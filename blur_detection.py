import tensorflow as tf
from tensorflow import keras
import os
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID";
os.environ["CUDA_VISIBLE_DEVICES"] = "0";

img_width = 224
img_height = 224
train_data_dir = '/home/vishwam/mountpoint/Gender/gender_class_v_1/train'
valid_data_dir = '/home/vishwam/mountpoint/Gender/gender_class_v_1/validation'
model_path = "Blur_face_31Mar_V_ep5.h5"
#filepath="gender_6f_weights-improvement-{epoch:02d}-{val_accuracy:.2f}.hdf5"
epochs=5
steps_per_epoch =262
validation_steps =444
loss='binary_crossentropy'
optimizer = tf.keras.optimizers.Adam(learning_rate=0.001)

datagen = tf.keras.preprocessing.image.ImageDataGenerator(rescale = 1./255)

train_generator = datagen.flow_from_directory(directory=train_data_dir,
											   target_size=(img_width,img_height),
											   classes=['blur','non_blur'],
											   class_mode='binary',
											   batch_size=32,interpolation='lanczos')

validation_generator = datagen.flow_from_directory(directory=valid_data_dir,
											   target_size=(img_width,img_height),
											   classes=['blur','non_blur'],
											   class_mode='binary',
											   batch_size=1,interpolation='lanczos')


# step-2 : build model

model =tf.keras.models.load_model(model_path)
#model.compile(loss=loss,optimizer=optimizer,metrics=['accuracy'])
#checkpoint = tf.keras.callbacks.ModelCheckpoint(filepath, monitor='val_accuracy', verbose=1, save_best_only=True, mode='max')
#early_stopping = keras.callbacks.callbacks.EarlyStopping(monitor='val_acc', min_delta=0, patience=2, verbose=0, mode='max', baseline=None, restore_b$
#callback_list = [checkpoint]


model.compile(loss=loss,optimizer=optimizer,metrics=['accuracy'])

print('model complied!!')

print('started training....')
training = model.fit_generator(generator=train_generator, steps_per_epoch=steps_per_epoch,epochs=epochs,validation_data=validation_generator,validation_steps=validation_steps)

print('training finished!!')

print('saving weights to h5')

model.save('Blur_face_1april_v5.h5',include_optimizer=False)

print('all weights saved successfully !!')
#models.load_weights('qqqqqqqsimple_CNN.h5')
#from tensorflow.contrib.session_bundle import exporter

#export_path = "tf_serving/" # where to save the exported graph
#export_version = 1 # version number (integer)
#
#saver = tf.train.Saver(sharded=True)
#model_exporter = exporter.Exporter(saver)
#signature = exporter.classification_signature(input_tensor=model.input,
#                                              scores_tensor=model.output)
#print(model.input)
#print(model.output)
#model_exporter.init(sess.graph.as_graph_def(),
#                    default_graph_signature=signature)
#model_exporter.export(export_path, tf.constant(export_version), sess)
#path_h5='without_motion_without_kushi_blurriness'+'/'
#path = 'tf_serving_blur_faces_v_4_0_Nima_Technical_BS_16/'
#model.save_model('without_motion_without_kushi_blurriness.h5',path_h5,include_optimizer=False,save_format='h5')
#keras.experimental.export_saved_model(model, path)

#config = model.get_config()
#model.save_weights('checkpoints_blur_faces_v_4_0_Nima_Technical_BS_16/', save_format='tf')

#tf.contrib.saved_model.save_keras_model(model, path)

